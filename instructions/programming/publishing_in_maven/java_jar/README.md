# Публикация Java библиотеки в [Maven Central](http://search.maven.org/)

`~/.m2/settings.xml`

```
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                        http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <servers>
        <server>
            <id>ossrh</id>
            <username>Имя пользователя</username>
            <password>Пароль</password>
        </server>
    </servers>

    <profiles>
        <profile>
            <id>gpg-passphrase</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <gpg.passphrase>Пароль ключа</gpg.passphrase>
            </properties>
        </profile>
    </profiles>

</settings>
```
