* Создание пары ключей - `gpg --gen-key`
* Получение списка ключей (что бы узнать ${GPG_KEY_ID}) - `gpg --list-secret-keys --keyid-format SHORT` или `gpg --list-secret-keys --keyid-format LONG`
* Экспорт приватных ключей - `gpg --export-secret-keys >$FILENAME` или `gpg --export-secret-key ${GPG_KEY_ID} >$FILENAME`
* Экспорт публичных ключей ключей - `gpg --export >$FILENAME` или `gpg --export ${GPG_KEY_ID} >$FILENAME`
* Импорт ключей - `gpg --import $FILENAME`
* Отправка ключа - `gpg --keyserver hkp://keyserver.ubuntu.com --send-keys ${GPG_KEY_ID}`
