`VERSION_NAME`=версия библиотеки (допускаются буквы, например -alpha), например `1.0.0`  
`VERSION_CODE`=код версии (только число), например `1`  
`GROUP`=`$GROUP_ID`  
  
`POM_DESCRIPTION`=`$LIBRARY_UI_NAME`  
`POM_URL`=Ссылка на репозиторий проекта, например `https://github.com/hnau256/HLog`  
`POM_SCM_URL`=То же, что и выше  
`POM_SCM_CONNECTION`=Ссылка на подключение к проекту, например `scm:git@github.com:hnau256/HLog.git`  
`POM_SCM_DEV_CONNECTION`=То же, что и выше  
`POM_LICENCE_NAME`=Название лицензии, например `The MIT License (MIT)`  
`POM_LICENCE_URL`=Ссылка на полный текст лицензии, например `http://opensource.org/licenses/MIT`  
`POM_LICENCE_DIST`=repo  
`POM_DEVELOPER_ID`=Ник пользователя в `Sonatype`  
`POM_DEVELOPER_NAME`=Имя и фамилия разработчика в `Sonatype` на английском